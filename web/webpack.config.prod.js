var webpack = require('webpack'),
		path = require('path');

var ExtractTextPlugin = require("extract-text-webpack-plugin");

var devConfig = require("./webpack.config.js");
var devConfig = Object.create(devConfig);
devConfig.watch=false;
devConfig.noParse=false;
devConfig.cache=false;
devConfig.plugins=[
	new webpack.ProvidePlugin({
		$: "jquery",
		jQuery: "jquery",
		"window.jQuery": "jquery",
		"moment":"moment"
	}),
	//REMOVE DUPLICATES
	new webpack.optimize.DedupePlugin(),

	//EXTRACT ALL CSS
	new ExtractTextPlugin(devConfig.bundleName+".css", {
		allChunks: true
	}),
	new webpack.optimize.CommonsChunkPlugin(/* chunkName= */"vendor", /* filename= */"vendor."+devConfig.bundleName+".js"),

	new webpack.optimize.UglifyJsPlugin({
		minimize:true,
		sourceMap:false,
		compress: {
			warnings: false
		},
		comments:false
	})
]

module.exports = devConfig;