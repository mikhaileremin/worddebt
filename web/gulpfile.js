var gulp = require("gulp");
var gutil = require("gulp-util");
var webpack = require("webpack");

var webpackConfig = require("./webpack.config.js");
var webpackConfigProd = require("./webpack.config.prod.js");

// The development server (the recommended option for development)
gulp.task("default", ["webpack:build"]);


gulp.task("webpack:build", function(callback) {
	// modify some webpack config options
	var myConfig = Object.create(webpackConfigProd);
	// run webpack
	webpack(myConfig, function(err, stats) {
		if(err) throw new gutil.PluginError("webpack:build", err);
		gutil.log("[webpack:build]", stats.toString({
			colors: true
		}));
		callback();
	});
});


gulp.task("dev", function(callback) {
	// modify some webpack config options
	var myConfig = Object.create(webpackConfig);
	// run webpack
	webpack(myConfig, function(err, stats) {
		if(err) throw new gutil.PluginError("webpack:build", err);
		gutil.log("[webpack:build]", stats.toString({
			colors: true
		}));
		callback();
	});
});

