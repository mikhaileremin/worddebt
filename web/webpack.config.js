var webpack = require('webpack'),
		path = require('path');

var ExtractTextPlugin = require("extract-text-webpack-plugin");

var APP = __dirname + '/';
var bundleName="worddebt";

module.exports = {
	context: APP,
	cache:true,
	bundleName:bundleName,
	noParse:/node_modules/, //do not parse 3rd party libs in development
	resolve: {
		root: [path.join(__dirname, 'src')],
		alias: {
			app: path.join('application/application'),
			angular: 'angular/angular.min',
			'angular-translate$': './libs/angular-translate/angular-translate.js',
			'angular-translate-loader':'./libs/angular-translate/angular-translate-loader-url.js',
			//moment$: 'moment/moment.js',
			"freewall":"./libs/freewall",
			"tooltip":"./libs/tooltip",
			"jquery-mousewheel":"./libs/jquery.mousewheel.min.js",
			"jHighlight":"./libs/jquery.highlight",
			"RoyalSlider":"./libs/royalslider/jquery.royalslider.min.js",
			"tagcanvas":"./libs/jquery.tagcanvas.min",
			"RoyalSliderCss":APP+"libs/royalslider/royalslider.css",
			"RoyalSliderThemeCss":APP+"libs/royalslider/skins/default-inverted/rs-default-inverted.css",
			"TweenLite":APP+"libs/greensock/uncompressed/TweenLite.js",
			"TimelineLite":APP+"libs/greensock/uncompressed/TimelineLite.js",
			"TweenMax":APP+"libs/greensock/uncompressed/TweenMax.js",
			"TweenEasing":"./libs/greensock/uncompressed/easing/EasePack.js",
			"TweenCss":"./libs/greensock/uncompressed/plugins/CSSPlugin.js",
			"TweenScrollTo":"./libs/greensock/uncompressed/plugins/ScrollToPlugin.js",
			"lodash":"./libs/lodash/lodash.min",
			"wheelIndicator":APP+"libs/wheel-indicator.js",
			"imagesLoaded":"./libs/imagesloaded.pkgd.min.js"
		}
	},
	"entry":{
		app:"./src/boot.js",
		vendor: [
			"jquery",
			"angular",
			"angular-cookies",
			"angular-translate",
			"angular-animate",
			"angular-messages",
			"angular-aria",
			"angular-sanitize",
			'angular-translate-loader',
			"angular-material",
			"tooltip",
			"moment",
			"jHighlight",
			"lodash",
			"RoyalSlider",
			"tagcanvas",
			"TimelineLite",
			"TweenScrollTo",
			"TweenLite",
			"TweenMax", //svg transitions won't work without tweenmax
			"TweenScrollTo",
			"TweenEasing",
			"TweenCss",
			"imagesLoaded",
			"jquery-mousewheel"
		]
	},
	plugins: [
		//PROVIDE GLOBAL JQUERY OF ALL FORMS (FOR PLUGINS)
		new webpack.ProvidePlugin({
			$: "jquery",
			jQuery: "jquery",
			"window.jQuery": "jquery",
			"moment":"moment"
		}),
		//WATCHER
		new webpack.OldWatchingPlugin(),

		//EXTRACT VENDOR CHUNK (chunk name, filename)
		new webpack.optimize.CommonsChunkPlugin("vendor", "vendor."+bundleName+".js"),

		//EXTRACT ALL CSS
		new ExtractTextPlugin(bundleName+".css", {
			allChunks: true
		})
	],
	module: {
		loaders: [
			//RAW HTML LOADER
			{
				test:/\.html$/,
				loader:'raw'
			},
			//HIGHLIGHT JS LOADER
			{
				test: /\.highlight/,
				loader: "imports?jQuery=jquery"
			},
			//JQUERY LOADER
			{
				test: /jquery\.js$/,
				loader: 'expose?jQuery',
			},
			//JQUERY LOADER
			{
				test: /wheel-indicator/,
				loader: 'expose?WheelIndicator!exports?WheelIndicator',
			},
			//Tooltip stolen from bootstrap3 :)
			{
				test:/libs\/tooltip\.js&/,
				loader:"imports?jQuery"
			},
			//CSS loader
			{
				test: /\.css$/,
				loader: ExtractTextPlugin.extract("style-loader", "css-loader")
			},
			//LESS LOADER
			{
				test: /\.less$/,
				loader: ExtractTextPlugin.extract("style-loader", "css-loader!less-loader")
			},

			//SMALL IMAGE LOADERS (BASE64)
			{ test: /\.png$/, loader: "url-loader?limit=100000" },
			{ test: /\.jpg$/, loader: "file-loader" },
			{ test: /\.gif/, loader: "file-loader" }
		]
	},
	output: {
		path: path.resolve(__dirname, "../public/build"),
		filename: bundleName+ '.js'
	}
}