var app = angular.module('WordDebtApplication',["ngAria","ngAnimate","ngSanitize","ngMaterial","ngCookies","pascalprecht.translate","ngMessages"]);
app.config(['$compileProvider', function ($compileProvider) {
	$compileProvider.debugInfoEnabled(false);
}]);

app.run(["$rootScope",function($rootScope){
	$rootScope.changeScreen=function(){
		$(".main-content .container").addClass("animated fadeOutUp")
	}
}]);

//Get locale cookie
function getCookie(name) {
	var cookie = " " + document.cookie;
	var search = " " + name + "=";
	var setStr = null;
	var offset = 0;
	var end = 0;
	if (cookie.length > 0) {
		offset = cookie.indexOf(search);
		if (offset != -1) {
			offset += search.length;
			end = cookie.indexOf(";", offset)
			if (end == -1) {
				end = cookie.length;
			}
			setStr = unescape(cookie.substring(offset, end));
		}
	}
	return(setStr);
}

app.config(['$translateProvider', function ($translateProvider) {
	// add translation table
	$translateProvider.useUrlLoader("/api/locale");
	var currentLocale=getCookie("WordDebtSelectedLocale")||"en";
	$translateProvider.preferredLanguage(currentLocale); //doesn't matter but it should be here to work
	moment.locale(currentLocale);
}]);


app.filter('trustsrc',["$sce",function($sce) {
	return function(val) {
		return $sce.trustAsResourceUrl(val);
	};
}]);

module.exports=app;