//index page handling
require("components/index/index");

//Tweets
require("components/profile/profile");

//Title directive
require("directives/title/title");

require("components/admin/admin");

require("directives/numinput/numinput");

//Donate page controller
require("components/donate/donate");

require("components/lang-switcher/lang-switcher");

//Main styles
require("styles/main.less");

//Load css for libraries
require("angular-material/angular-material.min.css");
require("RoyalSliderCss");
// require("RoyalSliderCssTheme");

//Bootstrap angular application on document
angular.bootstrap(document, ['WordDebtApplication'],{
	strictDi: true
});
