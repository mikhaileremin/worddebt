define([
	"app"
], function(app){
	"use strict";
	var collection = {
		videoLink:"https://www.youtube.com/embed/VKU_mA7MQdg"
	};
	app.constant("appConstants", collection);
	return collection;
});