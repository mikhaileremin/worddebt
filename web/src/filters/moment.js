define([
	'app',
	"moment"
], function (app,moment) {
	"use strict";
	app.filter('moment', [function () {
		return function (value,format) {
			if(!value)
				return "";
			return moment(value).format(format||"LL");
		}}]);
});

