define([
	'app',
], function (app) {
	"use strict";
	app.directive('wall', ["$timeout",function($timeout) {
		return function(scope, elm, attrs) {

			var $grid=new freewall(elm);
			var colW=250;

			var selector=attrs.wall||'.image';
			var waitForSpecialSelector=attrs.wait||false;
			var init=function(){
				$grid.reset({
					selector: selector,
					animate: true,
					cellW: colW,
					cellH:"auto"
					//gutterY: 10,
					//gutterX: 10
				});

				$grid.fitWidth();
				$grid.refresh();

				setTimeout(function(){
					elm.addClass("wall-initialized");
				},0)

				angular.element(window).on("resize",resizeHandler);
			}

			var resizeHandler=function(){
				$grid.refresh();
				$grid.fitWidth();
			}


			var unwatch=scope.$watch(function(){
				//wait for special thing(if we want something special), or just a common list item
				if((!waitForSpecialSelector && elm.find(selector).length) || (waitForSpecialSelector && elm.find(waitForSpecialSelector).length)){
					//Init with some delay left for rendering
					$timeout(function () {
						init();
					},0)
					//unwatch after init
					unwatch();
				}
			})

			scope.$on("$destroy",function(){
				$grid.destroy();
				angular.element(window).off("resize",resizeHandler);
			})
		};
	}]);
	return app;
});