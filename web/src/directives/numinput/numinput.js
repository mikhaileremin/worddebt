define([
	'app'
], function (app) {
	"use strict";
	var directive = [function () {
		return {
			restrict:"A",
			link: function (scope, elm, attr) {
				elm.on("keypress",function(e){
					return e.metaKey || // cmd/ctrl
						e.which <= 0 || // arrow keys
						e.which == 8 || // delete key
						(e.which == 46 && !~elm.val().indexOf(".")) ||
						/[0-9]/.test(String.fromCharCode(e.which)); // numbers
				})
			}
		};
	}];
	app.directive('numinput', directive);
	return app;
});