define([
	'app',
	"./title.less"
], function (app) {
	"use strict";
	app.directive('title', ["$timeout",function($timeout) {
		return function(scope, elm, attrs) {
			//let things render first
			var tooltipParams={
				trigger:"hover",
				container:"body",
				placement:attrs.tooltipPlacement||'top auto'
			}
			$timeout(function(){
				elm.tooltip(tooltipParams);
			},10)

			//If needed - look on changes
			if(angular.isDefined(attrs.observeTitle)){
				attrs.$observe('title', function() {
					elm.tooltip("destroy");
					elm.tooltip(tooltipParams);
				});
			}

			scope.$on("$destroy",function(){
				elm.tooltip("destroy");
			})
		};
	}]);
	return app;
});