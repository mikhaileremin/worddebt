define([
	"app"
], function(app){
	"use strict";
	//require wheel indicator
	var WheelIndicator=require("wheelIndicator");

	app.directive('pageScroller', [function(){
		return {
			restrict: 'A', // only activate on element attribute
			scope: {
				
			},
			link: function(scope, elm, attrs) {
				var simpleScrollPageHeightLimit=700,
					pageActivationOffset=$(window).height()/1.5;

				var mainLoadIndicator=$(".splash-screen");
				//cached elements
				var pages=$(attrs.pageScroller?attrs.pageScroller:".page"),
					scrollContainer=attrs.container?$(attrs.container):$(window);

				var pagesActivatedMap=[];
				angular.forEach(pages,function (page,i) {
					pagesActivatedMap[i]=false;
				})
				//states
				var changingPageNow=false;

				//numbers
				var activePageNumber=0,
					totalPages=pages.length,
					pageChangeSec=0.8,
					textAppearSec=1;



				var pagingWheelHandler=function (event,direction) {
					event.preventDefault();
					event.stopPropagation();
					changePage(~direction,event);
				}

				// var pagingWheelHandler=_.debounce(pagingWheelHandlerOrig,pageChangeSec,{leading:true});

				//Called when page is activated
				var onActivatePage=function (activeIndex) {
					//if page already activated - return
					if(pagesActivatedMap[activeIndex] || !pages[activeIndex])return;
					pagesActivatedMap[activeIndex]=true;

					//animate page
					switch (activeIndex){
						//First page already animated
						case 0:
							break;
						//Page what we can do
						case 1:
							TweenLite.fromTo($(pages[activeIndex]).find(".active-content"),
								textAppearSec,
								{
									opacity:0,
									y:"50%"
								},{
									opacity:1,
									y:"0%",
									ease:Expo.easeOut
								}
							);
							break;
						//page with formula slider
						case 2:

							//blue line with formula
							TweenLite.fromTo($(pages[activeIndex]).find(".top-line,.howitworks-copy"),
								textAppearSec,
								{
									opacity:0,
									y:"-50%"
								},{
									opacity:1,
									y:"0%",
									ease:Expo.easeOut,
								}
							);

							TweenMax.staggerFromTo($(pages[activeIndex]).find(".step-title-wr.active-content,.step-description.active-content"),textAppearSec,
								{
									opacity:0,
									y:"50%"
								},
								{
									opacity:1,
									y:"0%",
									delay:0.2,
									ease:Expo.easeOut
								},
								0.2);

							TweenMax.fromTo($(pages[activeIndex]).find(".paging"),textAppearSec,{
								opacity:0,
								y:"50%"
							},{
								opacity:1,
								y:"0%",
								delay:0.5,
								ease:Expo.easeOut
							})

							break;

						//map page
						case 3:
							//Map
							TweenLite.fromTo($(pages[activeIndex]).find(".map-wrapper"),
								textAppearSec,
								{
									opacity:0,
									scale:0.5
								},{
									opacity:1,
									scale:1,
									ease:Expo.easeOut,
								}
							);

							//Top text heading and subheading
							TweenLite.fromTo($(pages[activeIndex]).find(".toptext"),
								textAppearSec,
								{
									opacity:0,
									y:"-50%"
								},{
									opacity:1,
									y:"0%",
									delay:0.2,
									ease:Expo.easeOut
								}
							);

							//Bottom text (statistics)
							TweenLite.fromTo($(pages[activeIndex]).find(".stat-text"),
								textAppearSec,
								{
									opacity:0,
									y:"50%"
								},{
									opacity:1,
									y:"0%",
									delay:0.2,
									ease:Expo.easeOut
								}
							);

							break;
						//word cloud
						case 4:
							TweenLite.fromTo($(pages[activeIndex]).find(".container.active-content"),
								textAppearSec*2,
								{
									opacity:0,
									y:"50%"
								},{
									opacity:1,
									y:"0%",
									ease:Expo.easeOut,
									onComplete:function () {

									}
								}
							);
							TweenLite.to($(pages[activeIndex]).find(".text-heading .marker"),
								textAppearSec,{
									ease:Expo.easeOut,
									delay:textAppearSec,
									width:"110%"
								})
							break;

						//page with little girl (pay off your debt)
						case 5:
							TweenLite.fromTo($(pages[activeIndex]).find(".active-content"),
								textAppearSec,
								{
									opacity:0,
									y:"50%"
								},{
									opacity:1,
									y:"0%",
									ease:Expo.easeOut
								}
							);
							break;
					}

				}

				//Detect current page number by given scroll
				var detectPage=function () {
					var fromTop = $(scrollContainer).scrollTop() ;
					// Get id of current scroll item
					var cur = pages.map(function(i){
						if ($(this).offset().top <= fromTop + pageActivationOffset)
							return this;
					});
					// Get the id of the current element
					cur = cur[cur.length-1];

					var oldPageNumber=activePageNumber;
					activePageNumber=pages.index(cur);
					if(oldPageNumber!=activePageNumber){
						onActivatePage(activePageNumber);
					}
				}

				var throttledDetectPage=_.throttle(detectPage,500,{trailing:true});
				throttledDetectPage();


				//Handler for regular scroll event (NOT wheel)
				var simpleScrollHandler=function () {
					if(!changingPageNow){
						throttledDetectPage();
					}
				}


				//Change page
				var changePage=function (isGoBack,event) {

					//If already changing page - prevent scrolling and do nothing more
					if(changingPageNow){
						return;
					}
					var shouldScrollToBottom=false;
					//forward (scroll down)
					if(!isGoBack){
						if(activePageNumber<totalPages-1){
							activePageNumber++;

						}else{
							//if we scroll down after last page - let it be. see footer and partners with regular scrolling
							shouldScrollToBottom=true;
						}
					}else{
						//backward
						if(activePageNumber>0)
							activePageNumber--;
						else
							return;
					}
					var targetOffset=!shouldScrollToBottom?$(pages[activePageNumber]).offset().top:$(document).height();
					//Set flag true to block other scroll attempts
					changingPageNow=true;
					//Animate full page scrolling
					TweenLite.to(scrollContainer,
						pageChangeSec,
						{
							scrollTo:{y:targetOffset},
							ease:Power2.easeInOut,
							onComplete:function () {
								onActivatePage(activePageNumber);
								changingPageNow=false;
							}
						}
					);
				}



				var scrollIndicator;
				//Detect scroll mode by window height
				var detectScrollMode=function () {
					if($(window).height()<=simpleScrollPageHeightLimit){
						//Mouse wheel handler
						// scrollContainer.off("mousewheel",pagingWheelHandler);
						if(scrollIndicator){
							scrollIndicator.turnOff();
							scrollIndicator.destroy();
						}
					}else{
						// scrollContainer.on("mousewheel",pagingWheelHandler);

						scrollIndicator = new WheelIndicator({
							elem: scrollContainer.get(0),
							callback: function(e){
								if(e.direction=="down"){
									changePage();
								}else {
									changePage(true);
								}
							}
						});
					}
					pageActivationOffset=$(window).height()/1.5;
				}

				detectScrollMode(); //detect scroll mode initially


				//Animate first page
				var animateFirstPage=function () {
					var indicatorHideTime=0.5;
					//Remove indicator
					TweenLite.to(mainLoadIndicator,
						indicatorHideTime,
						{
							opacity:0,
							ease:Power1.easeIn,
							onComplete:function () {
								mainLoadIndicator.remove();
							}
						}
					);


					setTimeout(function () {
						$(".landing-page").addClass("loaded");
						TweenLite.fromTo($(pages[0]).find(".active-content"),
							pageChangeSec,
							{
								opacity:0,
								y:"50%"
							},{
								opacity:1,
								delay:indicatorHideTime+0.2,
								y:"0%"
							}
						);

					},indicatorHideTime);
					
					pagesActivatedMap[0]=true;




				}

				//Initialize
				var init=function () {
					$(pages[0]).imagesLoaded().always(function( ) {
						animateFirstPage();
					});
				}
				init();


				$(window).on("resize",detectScrollMode)

				//SCROLL WATCHER FOR REGULAR SCROLLS OR MOBILE
				scrollContainer.on("scroll",simpleScrollHandler);

				scope.$on("$destroy",function(){
					scrollContainer.off("scroll",simpleScrollHandler)
				})
			}
		};
	}]);
	return app;
});