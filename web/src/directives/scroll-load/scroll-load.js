define([
	"app"
], function(app){
	"use strict";

	app.directive('scrollLoad', [function(){

		return {
			restrict: 'A', // only activate on element attribute
			scope: {
				onLoad:"=scrollLoad"
			},
			link: function(scope, elm, attrs) {
				var limit=attrs.offset||100;
				var container=attrs.container?elm.find(attrs.container):elm;

				var lastScrollPos=0;
				var wheelHandler= function (event) {
					var pixelsToBottom=container[0].scrollHeight - container[0].scrollTop - container.height();

					if(lastScrollPos < container[0].scrollTop && pixelsToBottom < limit){
						scope.onLoad();
					}
					lastScrollPos=container[0].scrollTop;
				}

				//This event fired when scroll triggered by some external controls
				container.on("pseudoScroll",wheelHandler);
				elm.on("scroll",wheelHandler)

				scope.$on("$destroy",function(){
					elm.off("scroll",wheelHandler)
					container.off("pseudoScroll",wheelHandler);
				})
			}
		};
	}]);
	return app;
});