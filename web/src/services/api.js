define([
	"app",
], function(app){

	//SERVICE
	var service = ["$http",function($http){
		var apiUrl="/api";
		var _public={
			analyzeTweets:function(){
				return $http.get(apiUrl+"/analyzetweets");
			},
			analyzeFacebook:function(){
				return $http.get(apiUrl+"/analyzefacebook");
			},
			getJoinedCount:function(){
				return $http.get(apiUrl+"/peoplejoined");
			},
			getStats:function () {
				return $http.get(apiUrl+"/getstats");
			},
			getCloud:function () {
				return $http.get(apiUrl+"/getcloud");
			},
			getUsedWordsCount:function () {
				return $http.get(apiUrl+"/getwordscount");	
			},
			getDonations:function(){
				return $http.get(apiUrl+"/donations");
			},
			saveRoot:function(root){
				return $http.put(apiUrl+"/roots",root)
			},
			deleteRoot: function (root){
				return $http.delete(apiUrl+"/roots",{params:{_id:root._id}});
			},
			setLocale:function(locale){
				return $http.put(apiUrl+"/locale",{locale:locale})
			},
			getRoots:function(filter,start,limit){
				return $http.get(apiUrl+"/roots",{params:{
					filter:filter||'',
					start:start||0,
					limit:limit||0
				}});
			}

		}
		return _public;
	}];
	app.factory("api", service);
	return service;
});