define(["app","moment","services/api","filters/moment","./profile.less"],function(app,moment){
	var directive = [function () {
		return {
			restrict:"A",
			scope:{

			},
			template:require("./profile.html"),
			controller: ["$scope", "$element","api",function(scope,elm,api) {
				scope.tweets=[];
				scope.fbposts=[];
				scope.usedWordsArray=[];
				scope.error=false;
				scope.from=moment().subtract(7,"days");
				scope.to=new Date();

				scope.query="";
				scope.count=50;
				scope.loading=false;
				scope.analyzing=false;
				scope.loadingDonations=false;
				scope.donations=[];
				scope.totalDonated=0;
				scope.myDebt=0;
				scope.wordPrice=0.1;

				scope.result={
					total:0,
					matchingWords:{}
				}
				scope.matchingWords=[];

				scope.haveUserDonated=false;
				scope.lastDonation=false;
				scope.loadTweets=function(){
					scope.loading=true;
					scope.loadingDonations=true;
					scope.tweets=[];
					api.analyzeTweets().success(function(data){
						if(!data.tweets || !data.match){
							scope.loading=false;
							return;
						}
						scope.tweets=data.tweets.statuses;
						//if no last donation or if it's too old (we use 1980 year)
						var  tooOld=moment().subtract(10,"years");

						scope.haveUserDonated=data.lastDonation && !moment(data.lastDonation).isBefore(tooOld);
						scope.lastDonation=scope.haveUserDonated?data.lastDonation:false;
						scope.loading=false;

						angular.extend(scope.result.matchingWords,data.match.matchings);

						scope.result.total+=data.match.total;

						var wordsArray=data.match.matchings?Object.keys(data.match.matchings):[];
						scope.usedWordsArray.push.apply(scope.usedWordsArray,wordsArray);
						setTimeout(function(){
							$(elm.find(".tweets-container")).highlight(scope.usedWordsArray);
						},10);


					}).error(function(){
						scope.error=true;
						scope.loading=false;
					});


					api.getDonations().success(function(data){
						scope.donations=data;
						if(!scope.donations || !scope.donations.length){
							scope.haveUserDonated=false;
						}
						scope.loadingDonations=false;
						angular.forEach(scope.donations,function(d){
							scope.totalDonated+=parseFloat(d.transaction.AMT);
						})

						if(scope.myDebt<0)
							scope.myDebt=0;
					});
				}

				scope.loadFacebook=function(){
					api.analyzeFacebook().success(function(data){
						if(!data.posts || !data.match){
							scope.loading=false;
							return;
						}
						scope.fbposts=data.posts;
						//if no last donation or if it's too old (we use 1980 year)

						scope.result.total+=data.match.total;
						var wordsArray=data.match.matchings?Object.keys(data.match.matchings):[];
						scope.usedWordsArray.push.apply(scope.usedWordsArray,wordsArray);
						angular.extend(scope.result.matchingWords,data.match.matchings);
						setTimeout(function(){
							$(elm.find(".tweets-container")).highlight(scope.usedWordsArray);
						},10);
					}).error(function(){
						scope.error=true;
						scope.loading=false;
					});
				}


				scope.loadTweets();
				scope.loadFacebook();

			}]
		}
	}];
	app.directive('profile', directive);
})