define(["app","directives/page-scroller/page-scroller","collections/appConstants","./index.less"],function(app){
	var controller = ["$scope","$element","api","$timeout","$mdDialog","appConstants",function (scope,elm,api,$timeout,$mdDialog,appConstants) {
		var scrollingArea=$(window); //scrolling window itself
		var sectionScrollTolerance=150,
			sectionScrollAppearTolerance=150,
			mapStateChangeTimeout=5000,
			topMenuAppearTolerance=50; //part of page visible to show appear animation

		var topMenu=elm.find(".header-menu");
		var contentContainer=elm.find(".index-content");
		var lastPageContainer=elm.find('.page-payoff');

		var howItWorksSlider,howItWorksSliderElement;

		// Get count of joined people from api
		scope.statsLoaded=false;
		
		//Get users count
		api.getJoinedCount().success(function(data){
			scope.peopleJoined=data.count;
			elm.find(".people-joined-wr").addClass("ready");
		})

		//Get continent stats (users)
		api.getStats().success(function (data) {
			var template={na:0,sa:0,eu:0,as:0,af:0,oc:0};

			scope.stats={
				words:data.words,
				users:angular.extend({},angular.copy(template),data.users)
			}
			scope.statsLoaded=true;
		})


		//Get word cloud data
		api.getCloud().success(function (data) {

			var cloud=[];

			if(data && data.length){
				scope.hoveredWord=data[0];
			}

			scope.cloudLoaded=true;
			scope.wordsCloud= data;

			$timeout(function () {
				//Init 3d cloud
				var opts={
					textColour : '#878787',
					outlineColour : '#16A9E1',
					// depth : 0.75,
					weightFrom:"data-weight",
					// dragControl:true,
					clickToFront :600,
					weight:true,
					weightMode:"size",
					weightSizeMin:10,
					maxSpeed:0.1,
					weightSizeMax:70
				}

				if($(window).width()<768){
					opts.dragControl=true;
					opts.maxSpeed=0.05;
				}
				$('#wordCloudCanvas').tagcanvas(opts,'wordCloudItems');

			},10)


		})

		scope.wordSelected=function (word) {
			scope.hoveredWord=word;
			try{
				scope.$digest();
			}catch(err){}
		}

		//Get used words stats
		api.getUsedWordsCount().success(function (data) {
			scope.greekWordsUsedTotal= data.count;
		})

		var contentSections=["#whatwecando","#howitworks","#contact"];

		// Cache selectors
		var scrollItems = contentSections.map(function(cs){
				var item = $(cs);
				if (item.length) { return item; }
			});

		elm.find(".homelink").on("click",function(){
			scrollingArea.stop().animate({
				scrollTop: 0
			}, 300);
		})

		//SCROLL WATCHER
		scrollingArea.on("scroll",function(){

			// Get container scroll position
			var fromTop = $(this).scrollTop();

			//Toggle sticky heading (remove menu at last page)
			if((fromTop > contentContainer.offset().top - topMenuAppearTolerance) &&
				fromTop < lastPageContainer.offset().top){
				topMenu.addClass("visible");
			}else{
				topMenu.removeClass("visible");
			}

		});

		//CREATE SLIDER FOR HOW IT WORKS

		scope.activeSlideIndex=0;
		var initSlider=function () {
			howItWorksSliderElement=elm.find(".howitworks-slider");

			howItWorksSlider=howItWorksSliderElement.royalSlider({
				keyboardNavEnabled: true,
				arrowsNavAutoHide:true,
				sliderDrag:true,
				imageScalePadding:0,
				slidesSpacing:0,
				navigateByClick:false,
				autoplay: {
					// autoplay options go gere
					pauseOnHover:false,
					enabled: true,
					stopAtAction: true
				},
				fullscreen: {
					enabled: true,
					buttonFS:false,
					nativeFS: true
				},
				numImagesToPreload:2,
			}).data('royalSlider');

			howItWorksSlider.ev.on('rsAfterSlideChange', function(event) {
				scope.activeSlideIndex=howItWorksSlider.currSlideId;
				scope.$digest();
			});
			howItWorksSlider.stopAutoPlay();
		}
		initSlider();

		var mapContainer=elm.find(".map-wrapper");

		//Map state change repeater. Check if map state unlocked and do job
		var mapStateInterval=setInterval(function () {
			if(!mapStateLocked)
				mapContainer.toggleClass("toggled")
		},mapStateChangeTimeout)

		scope.goSlide=function (index) {
			if(howItWorksSlider){
				howItWorksSlider.goTo(index);
			}
		}

		//Is map state locked(hovered on stat labels)
		var mapStateLocked=false;

		//On mmouse enter - lock map state to hovered (people or words)
		scope.lockMapState=function (showFront) {
			mapStateLocked=true;
			if(showFront){
				mapContainer.removeClass("toggled")
			}else{
				mapContainer.addClass("toggled")
			}
		}

		//Unlock map state
		scope.unlockMapState=function () {
			mapStateLocked=false;
		}



		scope.showVideo=function (event) {
			var DialogController = ["$scope",function ($videoScope) {
				$videoScope.videoLink=appConstants.videoLink;
			}]
			$mdDialog.show({
				template:require(("./video-page.html")),
				controller: DialogController,
				clickOutsideToClose:true,
				targetEvent:event||false
			});
		}



		//DO THINGS ON DOCUMENT READY
		$(document).ready(function () {
			Ya.share2('share-widget', {
				hooks:{
					onready: function () {
						setTimeout(function () {
							$('.share-wrapper').addClass("ready");
						},100)

					},
				},
				theme:{
					image:"/img/post_twitter.png",
					services: 'facebook,twitter,gplus'
				}
			})
		})

	}];
	app.controller('indexPageController', controller);
})