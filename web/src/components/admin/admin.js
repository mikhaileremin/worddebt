define(["app","../../directives/scroll-load/scroll-load","./admin.less"],function(app){
	var directive = [function () {
		return {
			restrict:"A",
			replace:true,
			scope:{

			},
			template:require("./admin.html"),
			controller: ["$scope", "$element","api","$mdDialog",function(scope,elm,api,$mdDialog) {
				scope.roots=[];
				scope.loading=false;
				scope.editedRoot=false;
				scope.editedRootOriginal=false;
				scope.locales=["en","fr","de","es","ru"];
				var pageSize=20;

				scope.search=function(newSearch){
					if(scope.loading)return;

					scope.loading=true;

					api.getRoots(scope.searchFilter,newSearch?0:scope.roots.length,pageSize).success(function(data){
						//replace collection
						if(newSearch)
							scope.roots=data;
						// add to current list
						else
							scope.roots.push.apply(scope.roots,data);

						scope.loading=false;
					})
				}

				scope.search();


				//Map TMP values for nice ng-model using
				var mapTmpRoots=function(){
					scope.editedRoot.tmpRoots=scope.editedRoot.roots.map(function(r){
						return {value:r}
					})
					scope.editedRoot.tmpMatching=scope.editedRoot.matching.map(function(r){
						return {value:r}
					})

					angular.forEach(scope.locales,function(lang){
						if(!scope.editedRoot[lang]){
							scope.editedRoot[lang]=angular.copy(rootTemplate);
						}
						scope.editedRoot[lang].tmpRoots=scope.editedRoot[lang].roots.map(function(r){
							return {value:r}
						})
						scope.editedRoot[lang].tmpMatching=scope.editedRoot[lang].matching.map(function(r){
							return {value:r}
						})
					})
				}

				//Unmap ng-model mappings to simple array object
				var unmapTmpRoots=function(){
					scope.editedRoot.matching=scope.editedRoot.tmpMatching.map(function(m){
						return m.value;
					})

					scope.editedRoot.roots=scope.editedRoot.tmpRoots.map(function(m){
						return m.value;
					})

					//Map inside locales
					angular.forEach(scope.locales,function(lang){
						var localeRoot=scope.editedRoot[lang];

						if(localeRoot.tmpRoots)
							localeRoot.roots=localeRoot.tmpRoots.map(function(m){
								return m.value;
							})
						if(localeRoot.tmpMatching)
							localeRoot.matching=localeRoot.tmpMatching.map(function(m){
								return m.value;
							})
					})
				}

				//Delete tmp ng-model things from object-to-save
				var deleteTmpRoots=function(tmp){
					delete tmp.tmpMatching;
					delete tmp.tmpRoots;
					angular.forEach(scope.locales,function(l){
						delete tmp[l].tmpMatching;
						delete tmp[l].tmpRoots;
					})
				}


				scope.editRoot=function(root){
					scope.editedRoot=angular.copy(root);
					scope.editedRootOriginal=root;
					scope.selectLocaleTab("en");
					scope.inner.selectedLocaleTab=0;
					mapTmpRoots();
				}

				scope.inner={selectedLocaleTab:0};

				var rootTemplate={
					meaning:"",
					etymology:"",
					roots:[],
					matching:[],
					tmpMatching:[],
					tmpRoots:[]
				}
				scope.addRoot=function(){
					var newRoot=angular.copy(rootTemplate);
					scope.editRoot(newRoot);
				}

				scope.selectLocaleTab=function(locale){
					if(!locale || locale=="en"){
						scope.editedRootLocaleSection=scope.editedRoot;
						return
					}else{
						//if locale not exists in root
						if(!scope.editedRoot[locale]) {
							scope.editedRoot[locale] = angular.copy(rootTemplate);
						}
						scope.editedRootLocaleSection=scope.editedRoot[locale];
					}
				}

				scope.save=function(){

					if(scope.saving)return;

					scope.saving=true;

					//Unmap edited root tmp values to plain array
					unmapTmpRoots();

					//make copy
					var saveCopy=angular.copy(scope.editedRoot);

					//delete tmp data from copy
					deleteTmpRoots(saveCopy)

					//save to server
					api.saveRoot(saveCopy).success(function(){
						scope.saving=false;
						if(scope.editedRootOriginal._id){
							angular.extend(scope.editedRootOriginal,saveCopy);
						}else{
							scope.roots.push(saveCopy)
						}

						scope.editedRootOriginal=false;

					})
					scope.editedRoot=false;
				}

				scope.cancel=function(){
					scope.editedRoot=false;
					scope.editedRootOriginal=false;
				}

				//Validate form - primary valid is English
				scope.validate=function(){

					if(scope.editedRoot.tmpMatching &&	scope.editedRoot.tmpMatching.length)return true;

					var valid=false;

					angular.forEach(scope.locales,function(locale){
						if(!valid && scope.editedRoot[locale]
								&& scope.editedRoot[locale]
								&& scope.editedRoot[locale].tmpMatching
								&& scope.editedRoot[locale].tmpMatching.length){
							valid=true;
						}
					})
					return valid;
				}

				scope.delete=function($event){

					var dialog=$mdDialog.confirm()
							.title("Delete root")
							.content("Do you really want to delete root? This cannot be undone")
							.ok("Yes")
							.cancel("No")
							.targetEvent($event);

					$mdDialog.show(dialog).then(function(){
						var saveCopy=angular.copy(scope.editedRoot);
						deleteTmpRoots(saveCopy);
						api.deleteRoot(saveCopy);
						scope.roots.splice(scope.roots.indexOf(scope.editedRoot),1);
						scope.cancel();
					});

				}

				//Add item to some edited array and focus on last input
				scope.addItem=function(where,section){
					if(where)
						where.push({value:""});


					setTimeout(function(){
						elm.find(".items."+section+" input:last").focus();
					},0)

				}

				scope.removeItem=function(where,which){
					where.splice(where.indexOf(which),1);
				}
			}]
		}
	}];
	app.directive('admin', directive);
})