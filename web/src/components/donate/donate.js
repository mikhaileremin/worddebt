define(["app","./donate.less"],function(app){
	var controller = ["$scope","$element","api",function (scope,elm,api) {

		scope.donationSize=0;

		scope.donate=function(){
			window.location.href="/donate/pay?amount="+scope.donationSize;
		}
	}];
	app.controller('donatePageController', controller);
})