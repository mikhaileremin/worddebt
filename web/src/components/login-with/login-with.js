define([
	'app',
	"./login-with.less"
], function (app) {
	"use strict";
	var directive = ["$mdDialog","api",function ($mdDialog,api) {
		return {
			restrict: "A",
			replace: true,
			scope:{
			},
			link: function (scope, elm, attr ) {

				var clickHandler=function(event){
					var DialogController = ["$scope",function ($scope) {

					}]

					$mdDialog.show({
						template:require(("./login-with.html")),
						controller: DialogController,
						clickOutsideToClose:true,
						targetEvent:event||false
					});
				}


				elm.on("click",clickHandler);


				scope.$on("$destroy",function(){
					elm.off("click",clickHandler)
				})

			}
		}
	}];
	app.directive('loginWith', directive);
	return app;
});