define([
	'app',
	"moment",
	"./lang-switcher.less"
], function (app,moment) {
	"use strict";
	var directive = ["api","$cookies",function (api,$cookies) {
		return {
			restrict: "A",
			replace: true,
			scope:{
			},
			template:require( './lang-switcher.html'),
			link: function (scope, elm, attr ) {

				scope.locales=["en","fr","de","es","ru"];

				scope.activeLocale=$cookies.get("WordDebtSelectedLocale")||"en";

				scope.setLanguage=function(language){
					api.setLocale(language).success(function(){
						window.location.reload()
					});
				}

				scope.isActiveLang=function(lang){
					return scope.activeLocale==lang;
				}
			}
		}
	}];
	app.directive('langSwitcher', directive);
	return app;
});