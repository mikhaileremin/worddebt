
var schedule = require('node-schedule');

var User=require("../models/user");
var config=require("../config/config");

var _=require("lodash");

//var mongoose = require('mongoose');
//Connect to mongoose as mailer is separate process
//mongoose.connect(config.get('mongoose:uri'));

var Mailer=require("./mailer/mailer");



var rule = {};
_.extend(rule,config.get("cron").rule);
var jobToRun=function(){
	Mailer.notifyScheduled();
}
//CRON FOR UPDATING USER INFORMATION
var j = schedule.scheduleJob(rule, jobToRun);
module.exports = function(runNow){
	if(runNow==true)
		jobToRun();

	return j
}