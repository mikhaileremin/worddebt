var nodemailer = require('nodemailer');
var mailConfig=require("../../config/config.js").get("mailer");
var config=require("../../config/config.js");
var _=require("lodash");

var transportString='smtps://'+mailConfig.user+":"+mailConfig.password+"@"+mailConfig.host;
// create reusable transporter object using the default SMTP transport
var transporter = nodemailer.createTransport(transportString);
var fs=require("fs");

var Finder=require("../finder");

var ejs = require('ejs');
var template= fs.readFileSync('views/mail-template.ejs', 'utf8');
var i18n=require("i18n");

var mailer={
	notifyScheduled:function(){
		return Finder.findAndAnalyze(function(err,usersWithDebts){
			if (!err) {
				usersWithDebts.forEach(function(userWithDebt){

					if(!userWithDebt.user.email || !userWithDebt.user.email.length || !userWithDebt.total)return;

					var domain=config.get("protocol")+"://"+config.get("domain");

					if(userWithDebt.user.language){
						i18n.setLocale(userWithDebt.user.language)
					}

					var templateParams={
						count:userWithDebt.total,
						debt:(userWithDebt.total*config.get("wordprice")).toFixed(2),
						donateLink: domain+"/donate?count="+userWithDebt.total,
						user:userWithDebt.user,
						logo:domain+"/img/logo_WD_blue.png",
						logoSmile:domain+"/img/logo_smile_grey_mini.png",
						logoHeal:domain+"/img/healworld.png",
						paypalLogoUrl:domain+"/img/email_papal_logo_white_small.png",
						host:domain,
						matchings:_.toArray(userWithDebt.matchings),
						grayColor:"#ececec",
						themeColor:"#16A9E1",
						themeColorHover:"#16bbf3"
					}


					//RENDER EJS TO STRING
					var messageHtml = ejs.render(template, templateParams);

					//MAIL OPTIONS
					var mailOptions = {
						from: '"'+mailConfig.displayName+'" <'+mailConfig.user+'>', // sender address
						to: userWithDebt.user.email, // list of receivers
						subject: i18n.__("mailSubject"), // Subject line
						text: i18n.__("mailMessage"), // plaintext body
						html:  messageHtml // html body
					};

					//SEND MAIL AND SET LAST NOTIFICATION DATE AFTER SAVE
					transporter.sendMail(mailOptions, function(error, info){
						if(error){
							return console.log(error);
						}
						// console.log('Message sent to '+userWithDebt.user.email+' (' + info.response+")");
						userWithDebt.user.lastNotification=new Date(); //set last scan to now in order to skip this user until his turn(in 7 days, while cron is everyday)
						userWithDebt.user.save();
						return;
					});

				})

			} else {
				console.log("ERROR")
				res.statusCode = 500;
			}
			return;
		},true)
	},
	testLang: function () {
		//console.log(i18n.__("payoff"));
	}
}


module.exports = mailer;