'use strict';

var path = require('path');
var auth = require('../../config/auth');
var config = require('../../config/config');
var express = require('express');
var uuid = require('node-uuid');

var PayPal = require("./paypal-express-checkout");

var returnUrl = config.get("protocol")+"://"+config.get("domain")+"/donate/paypal/success";
var cancelUrl = config.get("protocol")+"://"+config.get("domain")+"/donate/paypal/cancel";

var Donation=require("../../models/donation");
var User       = require('../../models/user');

var apiUserName=config.get("paypal").username,
	apiPassword=config.get("paypal").password,
	apiSignature=config.get("paypal").signature,
	isDebug=config.get("paypal").debug;
var wordPrice=config.get("wordprice");

var dummyEmailAddress="test@example.com";

var router = express.Router();
var Twitter=require("../../modules/twitter");
var Facebook=require("../../modules/facebook");

var checkAuth=function(req,res,next){
	if(req.isAuthenticated()){
		next();
	}else{
		req.session.redirectTo=req.originalUrl; //REMEMBER THE GUY WHO CAME HERE UNATHORIZED (FROM MAIL) TO REDIRECT HIM BACK TO DONATE HE HE
		res.redirect('/auth');
	}
}


module.exports= function(app){

	router.get('/',checkAuth,function(req, res) {
		var viewParams={
			totalWords:req.query.count||0,
		}
		viewParams.total=(config.get("wordprice")*viewParams.totalWords).toFixed(2);

		res.render('donatepage.ejs',viewParams);
	});

	router.get('/pay',checkAuth,function(req, res) {
		// create paypal object in sandbox mode. If you want non-sandbox remove tha last param.
		var paypal = PayPal.create(apiUserName, apiPassword, apiSignature, isDebug);
		//paypal.setPayOptions('ACME Soft', null, process.env.logoImage, '00ff00', 'eeeeee');

		var payerEmail=(req.user && req.user.email)?req.user.email:(req.query.email?req.query.email:dummyEmailAddress);

		var moneyAmount=req.query.amount;
		paypal.setProducts([{
			name: 'Greek Words',
			description: 'Donation for saving children education in Greece',
			quantity: 1,
			amount: moneyAmount||1
		}]);

		// Invoice must be unique.
		var invoice = uuid.v4();
		paypal.setExpressCheckoutPayment(
			payerEmail,
			invoice,
			moneyAmount,
			'Donation for saving children education in Greece',
			'EUR',
			returnUrl,
			cancelUrl,
			false,
			function(err, data) {
				if (err) {
					console.log(err);
					res.status(500).send(err);
					return;
				}
				// Regular paid.
				res.redirect(data.redirectUrl);
		});
	});

	router.get('/paypal/cancel', function(req, res) {
		// Cancel payment.
		res.render('donateerror.ejs');
	});


	router.get('/paypal/success', checkAuth,function(req, res) {
		var paypal = PayPal.create(apiUserName, apiPassword, apiSignature, isDebug);
		paypal.getExpressCheckoutDetails(req.query.token, true, function(err, data) {
			if (err) {
				console.log(err);
				res.status(500).send(err);
				return;
			}

			var donationDate=new Date();
			var newDonation = new Donation({
				user:req.user._id,
				transaction:data,
				invoice:data.INVNUM,
				date:donationDate
			});

			//Post tweet about this donation
			if(req.user.twitter && req.user.twitter.id){
				Twitter.postDonationTweet();
			}

			//Post in facebook about this donation
			// if(req.user.facebook && req.user.facebook.id){
			// 	Facebook.postDonationMessage(req.user,function () {
			//		
			// 	});
			// }


			//Save donation to database
			newDonation.save(function(err) {
				if (err){
					res.render('donateerror.ejs');
					return;
				}
				//save donation date to user
				User.findOne({ "_id" : req.user._id }, function(err, user) {
					if (err)
						return;

					// if the user is found then save his last donation date
					if (user) {
						user.lastDonation=donationDate;
						user.save();
					}
				});

				// Show success message
				res.render('donatesuccess.ejs', {
					result : data, // get the user out of session and pass to template,
					user:req.user
				});
			})


		});
	});
	return router;
}