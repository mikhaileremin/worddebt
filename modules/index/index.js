'use strict';
var config = require('../../config/config');

var User       = require('../../models/user');


var i18n = require("i18n");
var differentIndexWordsCount=4;
module.exports= function(req, res) {
	var viewParams={
		userAuthorized:req.isAuthenticated(),
		randomWordIndex:(Math.random() * (differentIndexWordsCount-1)).toFixed()
	}


	if(req.user){
		viewParams.user=req.user
		viewParams.authProvider=req.session.authorizedWith||"twitter";
	}
	res.render('index.ejs',viewParams); // load the index.ejs file
}