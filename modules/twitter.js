var Twitter = require('twitter');
var configAuth=require("../config/auth");
var config=require("../config/config");

var client = new Twitter({
	consumer_key: configAuth.twitterAuth.consumerKey,
	consumer_secret: configAuth.twitterAuth.consumerSecret,
	access_token_key: configAuth.twitterAuth.accessToken,
	access_token_secret: configAuth.twitterAuth.accessTokenSecret
});

var i18n = require("i18n");

var tooOld="1980-01-01";

var donateMessage=i18n.__("donateTweet");

var TwitterApi={
	hashTag:"#WordDebt",
	getTweets:function(user,callback){
		var count=config.get("tweetsAnalyzeMax");
		var since=user.lastDonation?new Date(user.lastDonation):new Date(tooOld);

		var query={
			count:count,
			q:"from:"+user.twitter.username+" since:"+since.toISOString()
		}
		client.get('search/tweets', query, callback);
	},
	postDonationTweet: function (callback) {
		callback=callback||function(){return};
		client.post('statuses/update', {status:donateMessage+" "+this.hashTag}, callback);
	}
}

module.exports = TwitterApi;