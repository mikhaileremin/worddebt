var fs=require("fs");
var _=require("lodash");

var htmlString=fs.readFileSync('./tmp/rootsDe.html',{encoding:"utf8"});

var config      = require('../config/config');
var GreekRoots = require('../models/GreekRoots');
var mongoose = require('mongoose');
mongoose.connect(config.get('mongoose:uri'));
//console.log($("table.wikitable tr td").text())

var jsdom  = require('jsdom');
var fs     = require('fs');

var itemTemplate={
	roots:[], //roots
	meaning:"",//meaning
	etymology:"", //etymology (root origin)
	matching:[] //
}

jsdom.env({
	html: htmlString,
	scripts: ["./node_modules/jquery/dist/jquery.min.js"],
	done: function (err, window) {
		var $ = window.$;
		var foundRoots=[];
		var words=0;
		console.log("START PARSING DUTCH HTML")

		var separators = [",",";","\n\t\t","→"];
		var splitRegExp=new RegExp('[' + separators.join('') + ']', 'g');

		$("table.wikitable tr").each(function(i,tr){

			//It's header
			if(($(tr).attr("style") && $(tr).attr("style").length)){
				return;
			}
			var tds=$(tr).find("td");
			var roots=$(tds[0]).text().split(splitRegExp).map(function(r){
				return r.trim()
			}).filter(function (r) {
				return r && r.length
			})
			var matching=$(tds[3]).text().split(splitRegExp).map(function(r){
				return r.trim()
			}).filter(function (r) {
				return r && r.length
			})

			//ignore empty
			if(!roots.length && !matching.length)return;

			var obj={
				roots:roots, //roots
				meaning:$(tds[2]).text().trim(),//meaning
				etymology:$(tds[1]).text().trim(), //etymology (root origin)
				matching:matching //English examples
			}
			foundRoots.push(obj
			);



		});
		var done=function(err,result){
			console.log("DONE")
		}


		foundRoots.forEach(function(greekroot,i){
			// save our user into the database


			var splitted=greekroot.etymology.split(splitRegExp).filter(function (w) {
				return w.length
			});
			var q={
				$or:[]
			}
			splitted.forEach(function (etymItem) {
				q.$or.push({"etymology":{
					$regex:etymItem,
					$options:"i"
				}
				})
			})


			GreekRoots.find(q,function (err,found){

				//If match found - add FRENCH else - add new root
				if(found && found.length){
					//ADD FRENCH
					 found[0].de= _.extend({},itemTemplate,greekroot);

					console.log("FOUND MATCHING ROOT. EXTENDING IT",greekroot.meaning)
					found[0].save(function(err) {
						if (err)
							throw err;
						return done(null, newRoot);
					});
				}else{
					//NEW RECORD
					var newRoot   = new GreekRoots({
						etymology:greekroot.etymology
					});
					newRoot.de= _.extend({},itemTemplate,greekroot);
					newRoot.save(function(err) {
						if (err)
							throw err;
						return done(null, newRoot);
					});
				}
			})
			return;
		})
	}
});