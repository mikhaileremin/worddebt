var fs=require("fs");

var htmlString=fs.readFileSync('../tmp/rootsFr.html',{encoding:"utf8"});

var config      = require('../config/config');
var GreekRoots = require('../models/GreekRoots');


//console.log($("table.wikitable tr td").text())

var jsdom  = require('jsdom');
var fs     = require('fs');

jsdom.env({
	html: htmlString,
	scripts: ["./node_modules/jquery/dist/jquery.min.js"],
	done: function (err, window) {
		var $ = window.$;
		var table=[];
		var words=0;
		$("table.wikitable tr").each(function(i,tr){
			//It's header
			if(i==0)return;
			var tds=$(tr).find("td");

			if($(tds[2]).text()=="Greek"){
				var roots=$(tds[0]).text().split(",").map(function(r){
					return r.trim()
				})
				var matching=$(tds[4]).text().split(",").map(function(r){
					return r.trim()
				})
				table.push({
					roots:roots, //roots
					meaning:$(tds[1]).text(),//meaning
					etymology:$(tds[3]).text(), //etymology (root origin)
					matching:matching //English examples
				})
				words+=$(tds[4]).text().split(",").length
			};
		});
		var done=function(err,result){
			console.log("DONE")
		}

		table.forEach(function(greekroot,i){
			var newRoot   = new GreekRoots(greekroot);

			// save our user into the database
			newRoot.save(function(err) {
				if (err)
					throw err;
				return done(null, newRoot);
			});
		})
	}
});