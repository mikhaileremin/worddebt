
var User = require('../models/user');
var Twitter=require("../modules/twitter");
var Facebook=require("../modules/facebook");

var config=require("../config/config");
var moment=require("moment");

var GreekRoots = require('../models/GreekRoots');
var WordUsage = require('../models/WordUsage');
var tooOld="1980-01-01";
var async=require("async");
var _=require("lodash");

var Finder={
	//FIND USERS DEBTS. SEARCH TWEETS FROM LAST DONATION DATE AND ANALYZE'EM
	findAndAnalyze:function(afterAllCallback,extractStats){
		var self=this;
		//Find users that had to be scanned for words (user's turn is each X days - from config)
		return User.findScheduledUsers(function(err,users){
			//For each needed user find tweets since last user's donation
			var usersWithDebts=[];

			//FOR EACH FOUND USER DO ASYNC ANALYZING
			async.each(users,function(user,callback){
				var asyncDoneCount= 0,
					asyncNeedCount=0;
				var textsToAnalyze=[];
				//Callback for posts loader(facebook or twitter or both)

				var iManReady=function(err,texts,currentUser){
					textsToAnalyze=textsToAnalyze.concat(texts);
					asyncDoneCount++;

					//If they are equal - it means we have all we need and can analyze texts
					if(asyncDoneCount==asyncNeedCount){
						textsToAnalyze=textsToAnalyze.filter(function(text){
							if(text && text.length && !~text.indexOf(Twitter.hashTag))
								return true
							else
								return false
						})

						GreekRoots.findMatching(textsToAnalyze.join(" "),function (err ,foundRoots) {
							if (!err) {
								usersWithDebts.push(_.extend({},{user:Object.create(currentUser)},Object.create(foundRoots)));
								callback();
								return;
							} else {
								callback(true);
								return;
							}
						})

					}else{
						if(err){
							return;
						}
					}
				}

				//If user has twitter account connected - fetch tweets and callback'em as plain text array
				if(user.twitter && user.twitter.id){
					asyncNeedCount++;
					//Get tweets for given period and user
					Twitter.getTweets(user,function(err,tweets){
						if(err){
							iManReady(err,[],user);
						}else{
							iManReady(err,(tweets && tweets.statuses)?tweets.statuses.map(function (tweet) {return tweet.text;}):[],user);
						}
					})
				}

				//If user has facebook account connected - fetch fb posts and callback'em as plain text array
				if(user.facebook && user.facebook.id){
					asyncNeedCount++;
					Facebook.getPosts(user,function(posts){
						iManReady(false,posts.data?posts.data.map(function(post){return post.message}):[],user);
					})
				}

				//if user has nor facebook nor twitter - just skip
				if((!user.facebook || !user.facebook.id) && (!user.twitter || !user.twitter.id)){
					callback(true);
				}
			//AFTER ALL CALLBACK
			},function(err,done){

				if(!err){
					//extract word usage statistics
					if(extractStats){
						self.updateStats(usersWithDebts);
					}
					afterAllCallback(false,usersWithDebts);
				}
				else
					afterAllCallback(true);
			})
		})
	},
	updateStats:function (usersWithDebts) {
		//For each user get his words and update statistics
		usersWithDebts.forEach(function (u) {
			_.each(u.matchings,function (match,key) {

				var continent=(u.user && u.user.continent_code)?u.user.continent_code.toLowerCase():"eu";
				var incrementator={
					$inc:{
						total:match.count
					},
					meaning:match.meaning,
					etymology:match.etymology
				}
				incrementator.$inc["continents."+continent]=match.count;
				//Update word if exist
				WordUsage.update({word:match.word},incrementator,function (err,rowsAffected,response) {
					//error handler

					if(err){
						return err;
					}else{
						//new word stat
						if(!rowsAffected){
							var newStatWord=new WordUsage();
							newStatWord.word=match.word;
							newStatWord.meaning=match.meaning;
							newStatWord.etymology=match.etymology;
							newStatWord.continents={};
							newStatWord.total=match.count;
							newStatWord.continents[continent]=match.count;
							newStatWord.save();
						}
					}

				})
			})

		})
	}
}
module.exports=Finder;
