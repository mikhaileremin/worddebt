var FB = require('fb');
var configAuth=require("../config/auth");
var config=require("../config/config");

var i18n = require("i18n");
var donateMessage=i18n.__("donateTweet");

FB.options({
	appId:          configAuth.facebookAuth.clientID,
	appSecret:      configAuth.facebookAuth.clientSecret,
	redirectUri:    configAuth.facebookAuth.callbackURL
});

//basic app access
FB.setAccessToken(configAuth.facebookAuth.clientID+"|"+configAuth.facebookAuth.clientSecret);

var WorddebtFacebookApi={
	hashTag:"#WordDebt",
	options:FB.options(),
	accessToken:false,
	//Active access token for private things, e.g. for posting somethin
	refreshAccessToken:function(callback){
		if(this.accessToken){
			FB.setAccessToken(this.accessToken);
			callback(this.accessToken);
			return;
		}

		//Generated access token
		FB.api('oauth/access_token', {
			client_id: configAuth.facebookAuth.clientID,
			client_secret: configAuth.facebookAuth.clientSecret,
			grant_type: 'client_credentials'
		},function(res){
			if(!res || res.error) {
				console.log(!res ? 'error occurred' : res.error);
				callback();
				return
			}
			FB.setAccessToken(res.access_token);
			callback (res.access_token);
			return
		});
	},
	//Get user's posts
	getPosts:function(user,callback){
		var count=config.get("fbPostsAnalyzeMax");
		if(!user){
			callback({"error":"You are not authorized or doesn't have a facebook account"});
			return;
		}

		var sendParams={};
		if(user.lastDonation){
			sendParams["since"]=new Date(user.lastDonation).getTime()/1000;
		}
		FB.api("/"+user.facebook.id+"/feed","get",sendParams,function(res){
			if(!res || res.error || res.message) {
				console.log(!res ? 'error occurred' : res.error);
				callback(res);
				return;
			}
			callback(res);
		})
	},
	postDonationMessage:function (user,callback) {
		callback=callback||function () {};

		var domain=config.get("protocol")+"://"+config.get("domain")
		var data={
			'message' : donateMessage,
			'picture': domain+"/img/logo_WD_blue.png",
			'link' : domain
		}

		var donateToGroupId="1720226938262175";

		// FB.api("/"+user.facebook.id+"/feed","POST",data,function(res){
		// 	if(!res || res.error || res.message) {
		// 		console.log(!res ? 'error occurred' : res.error);
		// 		callback(res);
		// 		return;
		// 	}
		// 	callback(res);
		// })

		// FB.api("/"+donateToGroupId+"/feed","POST",data,function(res){
		// 	if(!res || res.error || res.message) {
		// 		console.log(!res ? 'error occurred' : res.error);
		// 		callback(res);
		// 		return;
		// 	}
		// 	callback(res);
		// })

	}
}

module.exports = WorddebtFacebookApi;