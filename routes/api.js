var express = require('express');
var router = express.Router();
var _= require("lodash");
var Finder=require("../modules/finder");
var Twitter=require("../modules/twitter");
var Facebook=require("../modules/facebook");

var Donation=require("../models/donation");
var passport=require("passport");
var config = require('../config/config');
var GreekRoots = require('../models/GreekRoots');
var User = require('../models/user');

var WordUsage = require('../models/WordUsage');

var checkAuth = require('../config/auth').checkAuth;
var checkSuperAuth = require('../config/auth').checkSuperAuth;

var i18n = require("i18n");

var rootLocalesList=["ru","fr","es","de"];
var async=require("async");



/* GET words listing. */
router.get('/roots',checkSuperAuth,function(req, res, next) {

  var q={};
  if(req.query.filter && req.query.filter.length){
    q["$or"]=[];

    q.$or.push({"meaning":{
      $regex:req.query.filter,
      $options:"i"
      }
    })



    var regex=new RegExp(req.query.filter);
    q.$or.push({"matching":{
        $in:[regex]
      }
    })

    q.$or.push({"roots":{
      $in:[regex]
    }})


    //Search locale specific
    q.$or.push({"ru.meaning":{
      $regex:req.query.filter,
      $options:"i"
    }})
    q.$or.push({"es.meaning":{
      $regex:req.query.filter,
      $options:"i"
    }})
    q.$or.push({"de.meaning":{
      $regex:req.query.filter,
      $options:"i"
    }})
    q.$or.push({"fr.meaning":{
      $regex:req.query.filter,
      $options:"i"
    }})
    q.$or.push({"ru.matching":{
      $in:[regex]
    }})
    q.$or.push({"es.matching":{
      $in:[regex]
    }})
    q.$or.push({"fr.matching":{
      $in:[regex]
    }})
    q.$or.push({"de.matching":{
      $in:[regex]
    }})
  }
  GreekRoots.find(q)
      .limit(req.query.limit||0).skip(req.query.start||0).exec(function (err, result) {
    if (!err) {
      return res.send(result);
    } else {
      res.statusCode = 500;
      return res.send({ error: 'Server error' });
    }
  });
});


/* Put roots listing. */
router.delete('/roots',checkSuperAuth,function(req, res, next) {
  if(req.query._id){
    GreekRoots.remove({_id:req.query._id},function (err) {
      if (!err) {
        return res.send({"ok":1});
      } else {
        res.statusCode = 500;
        return res.send({ error: 'Server error' });
      }
    })
  }

});

/* Put roots listing. */
router.put('/roots',checkSuperAuth,function(req, res, next) {
  if(req.body._id){
    GreekRoots.update({_id:req.body._id},req.body,function (err, updatedCount) {
      if (!err) {
        return res.send(req.body);
      } else {
        res.statusCode = 500;
        return res.send({ error: 'Server error' });
      }
    })
  }else{
    var newRoot= new GreekRoots();
    _.extend(newRoot,req.body);
    newRoot.save(function(err,nr){
      if (!err) {
        return res.send(nr);
      } else {
        res.statusCode = 500;
        return res.send({ error: 'Server error' });
      }
    })
  }

});

/* GET words listing. */
router.get('/donations',checkAuth,function(req, res, next) {
  Donation.find({user:req.user._id},function(err,donations){
    if (!err) {
      return res.send(donations);
    } else {
      res.statusCode = 500;
      return res.send({ error: 'Server error' });
    }
  });
});


router.get('/locale',function(req, res, next) {
  res.send(i18n.getCatalog(req.cookies[config.get("localeCookieName")]||"en"));
});


router.put('/locale',function(req, res, next) {
  i18n.setLocale(req.body.locale);
  res.cookie(config.get("localeCookieName"), req.body.locale, { maxAge: 900000 });
  if(req.user && req.user._id){
    req.user.language=req.body.locale;
    req.user.save();
  }


    res.send({"ok":1})
});

//Get statistics for mainpage map
router.get("/getstats",function (req,res) {



  //Users by continents

  var readyCount=0;

  var finalResult={users:{},words:{}};
  var checkIfReadyAndGo=function () {
    readyCount++;
    if(readyCount==2){
      res.send(finalResult)
    }
  }

  var pipe={
    $group: {
      _id:"$continent_code",
      total: {$sum : 1 }}
  }

  User.aggregate(pipe,function (err,result) {
    if (!err) {
      result.forEach(function (item,key) {
        if(item._id && item.total){
          finalResult.users[item._id.toLowerCase()]=item.total;
        }
      })
    }
    checkIfReadyAndGo();
  })

  var pipeContinents={
    $group:{
      _id: "",
      "eu": { $sum:"$continents.eu" },
      "as": { $sum:"$continents.as" },
      "af": { $sum:"$continents.af" },
      "na": { $sum:"$continents.na" },
      "sa": { $sum:"$continents.sa" },
      "oc": { $sum:"$continents.oc" }
    }
  }
  WordUsage.aggregate(pipeContinents,function (err,result) {
    if (!err) {
      finalResult.words=result[0];
    }
    checkIfReadyAndGo();
  });

})

router.get("/getcloud",function (req,res) {
  var cloudWordsLimit=20;
  WordUsage.find().sort({
    total:"desc"
  }).limit(cloudWordsLimit).exec(function (err,results) {
    res.send(results)
  });

})

router.get("/getwordscount",function (req,res) {
  WordUsage.count(function(err,count){
    if (!err) {
      return res.send({count:count});
    } else {
      res.statusCode = 500;
      return res.send({ error: 'Server error' });
    }
  });

})

// router.get("/testfb",function (req,res) {
//   Facebook.postDonationMessage(req.user,function (data) {
//     res.send({ok:data})
//   })
// })


//Get total users count
router.get('/peoplejoined',function(req, res, next) {
  User.count(function(err,count){
    if (!err) {
      return res.send({count:count});
    } else {
      res.statusCode = 500;
      return res.send({ error: 'Server error' });
    }
  });
});


//analyzes facebook posts, if any
router.get("/analyzefacebook",checkAuth,function(req,res,next){
  if(!req.user.facebook || !req.user.facebook.id){
    res.send({"error":"Don't have facebook account connected"})
    return;
  }

  Facebook.getPosts(req.user,function(posts){

    var texts=[];
    var result={};
    if(!posts.data)
      posts.data=[];
    posts.data=posts.data.filter(function(post){
      if(!post.message)return false;

      var notSpecial=!~post.message.indexOf(Twitter.hashTag);
      if(notSpecial && post.message)
        texts.push(post.message);

      return notSpecial
    });

    result.posts=posts.data;

    GreekRoots.findMatching(texts.join(" "),function (err ,greekRoots) {
      if (!err) {
        result.match=greekRoots;
        res.send(result);
      } else {
        res.statusCode = 500;
        return res.send({ error: 'Server error' });
      }
    })
  })
})


//analyzez tweets, if any
router.get("/analyzetweets",checkAuth,function(req,res,next){

  if(!req.user.twitter || !req.user.twitter.id){
    res.send({"error":"Don't have twitter account connected"})
    return;
  }

  Twitter.getTweets(req.user,function(error, tweets, response){
    var result={};
    if (!error) {
      var texts=[];
      //Filter special tweets with donation text
      tweets.statuses=tweets.statuses.filter(function(tweet){
        var notSpecial=!~tweet.text.indexOf(Twitter.hashTag);
        if(notSpecial)
          texts.push(tweet.text);

        return notSpecial
      });

      result={lastDonation:req.user.lastDonation,tweets:tweets};

      GreekRoots.findMatching(texts.join(" "),function (err ,greekRoots) {
        if (!err) {
          result.match=greekRoots;
          res.send(result);
        } else {
          res.statusCode = 500;
          return res.send({ error: 'Server error' });
        }
      })
    }
  })
})


// 'cause i am the master! show me your debts!
router.get("/showmetheirdebts",checkSuperAuth,function(req,res,next){
  Finder.findAndAnalyze(function(err,results){
    if (!err) {
      return res.send(results);
    } else {
      res.statusCode = 500;
      return res.send({ error: 'Server error' });
    }
  },true)
})

module.exports = router;
