var express = require('express');

var router = express.Router();
var moment=require("moment");

var indexModule=require("../modules/index/index");
var config=require("../config/config");

var adminEmails=config.get("security").admins;
var _=require("lodash");
var i18n = require("i18n");

var Finder=require("../modules/finder");

module.exports = function(app){
// route for home page
	router.get('/', indexModule);

// route for showing the profile page
	router.get('/profile', isLoggedIn, function(req, res) {
		res.render('profile.ejs', {
			userJoinedDate:moment(req.user.created).format("DD MMMM, YYYY"),
			authProvider:req.session.authorizedWith||"twitter",
			user : req.user // get the user out of session and pass to template
		});
	});
	//
	// router.get('/testmail',function(req, res) {
	// 	var domain=config.get("protocol")+"://"+config.get("domain");
	// 	Finder.findAndAnalyze(function(err,usersWithDebts){
	// 		var userWithDebt=usersWithDebts[0]||{};
	// 		var templateParams={
	// 			count:userWithDebt.total,
	// 			debt:(userWithDebt.total*config.get("wordprice")).toFixed(2),
	// 			donateLink: domain+"/donate?count="+userWithDebt.total,
	// 			user:userWithDebt.user,
	// 			logo:domain+"/img/logo_WD_blue.png",
	// 			logoSmile:domain+"/img/smile-fade.png",
	// 			logoHeal:domain+"/img/healworld.png",
	// 			paypalLogoUrl:domain+"/img/email_papal_logo_white_small.png",
	// 			host:domain,
	// 			matchings:_.toArray(userWithDebt.matchings),
	// 			layout:"blanklayout",
	// 			grayColor:"#ececec",
	// 			themeColor:"#16A9E1",
	// 			themeColorHover:"#16bbf3"
	// 		}
	// 		res.render('mail-template.ejs', templateParams);
	// 	})
	//
	// });

	router.get('/wordadmin', isSuperAdmin,function(req, res) {
		res.render('wordadmin.ejs', {
			authProvider:req.session.authorizedWith||"twitter",
			user : req.user // get the user out of session and pass to template
		});
	});


	// route middleware to make sure a user is logged in
	function isLoggedIn(req, res, next) {

		// if user is authenticated in the session, carry on
		if (req.isAuthenticated()){
			next();
			return true;
		}

		//Else remember where to go after login
		req.session.redirectTo=req.originalUrl; //REMEMBER THE GUY WHO CAME HERE UNATHORIZED (FROM MAIL) TO REDIRECT HIM BACK TO DONATE HE HE
		res.redirect('/auth');
		return false;
	}


	function isSuperAdmin(req, res, next){
		if(!isLoggedIn(req, res, next))return false;

		if(~adminEmails.indexOf(req.user.email)){
			next();
			return true;
		}else{
			res.redirect('/auth/error');
			return false;
		}
	}
	return router;
}