var express = require('express');
var passport       = require('passport');
require('passport-twitter');
require('passport-facebook');
var router = express.Router();
// In your js file (e.g. app.js)
var get_ip = require('ipware')().get_ip;
var satelize = require('satelize');

var userPermissions={scope:["email","user_posts" /*,"publish_actions"*/],authType: 'rerequest'};
module.exports = function(app){
// route for logging out
	router.get('/logout', function(req, res) {
		req.logout();
		res.redirect('/');
	});

	var getReturnUrl=function(req){
		var redirectTo=req.session.redirectTo;
		req.session.redirectTo=false;
		//If no redirect to given - go profile
		if(!redirectTo || !redirectTo.length){
			redirectTo="/profile"
		}
		return redirectTo;
	}

	var onAfterAuth=function (req,res) {
		if(!req.user.continent_code){
			var ipInfo=get_ip(req);
			if(ipInfo.clientIp){
				try{
					satelize.satelize({ip:ipInfo.clientIp}, function(err, payload) {
						if(payload && payload.continent_code){
							req.user.continent_code=payload.continent_code.toLowerCase();
							req.user.save();
						}
					});
				}catch(err){
					console.log("ERROR OCCURED WHILE IDENTIFYING USER'S GEO LOCATION BY IP",err)
				}

			}
		}

		res.redirect(getReturnUrl(req));
	}

	var authenticateTwitter=function(req,res,next){

		return passport.authenticate('twitter', {
			failureRedirect : '/'
		})(req,res,next)
	}

	var authenticateFacebook=function(req,res,next){
		var authParams=Object.create(userPermissions);
		authParams.failureRedirect ='/';
		//save auth provider type to session
		return passport.authenticate('facebook', authParams)(req,res,next)
	}


// =====================================
// TWITTER ROUTES ======================
// =====================================
// route for twitter authentication and login


	router.get('/auth/twitter', passport.authenticate('twitter'));

// handle the callback after twitter has authenticated the user
	router.get('/auth/twitter/callback',
			authenticateTwitter,function(req,res){
				//save auth provider type to session
				req.session.authorizedWith="twitter";
				onAfterAuth(req,res);
			});

	//FACEBOOK AUTH
	router.get('/auth/facebook',
			passport.authenticate('facebook',userPermissions));

	router.get('/auth/facebook/callback',
			authenticateFacebook,
			function(req, res) {
				//save auth provider type to session
				req.session.authorizedWith="facebook";
				onAfterAuth(req,res);
			});

	router.get('/auth/error', function(req, res) {
		res.render('autherror.ejs');
	});

	router.get('/auth', function(req, res) {
		res.render('connectwith.ejs');
	});
	return router;
}