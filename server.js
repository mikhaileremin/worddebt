var express = require('express');
var session=require("express-session");
var path = require('path');
var config      = require('./config/config');
var expressLayouts = require('express-ejs-layouts');
var mongoose = require('mongoose');
var flash        = require('req-flash');

var MongoDBStore = require('connect-mongodb-session')(session);

var app = express();

var i18n = require("i18n");
//CONNECT TO DATABASE
mongoose.connect(config.get('mongoose:uri'));

//EXPRESS APP
var app = express();

//LOAD AUTH CONFIG

//COOKIE AND BODY PARSER
var cookieParser = require('cookie-parser');
app.use(cookieParser());


//SESSION

var mongoSessionStore = new MongoDBStore(
	{
		uri: config.get("mongoose").uri
	});

app.use(session({
	resave: false,
	cookie: { secure: false },
	saveUninitialized: false,
	store:mongoSessionStore,
	secret: config.get("sessionAccessTokenSecret")
}));

//Flash messages
app.use(flash({locals: 'flash'}));


var passportConfig=require('./config/passport')(app);

//VIEW CONFIGURATION

//set up ejs for templating
app.set('view engine', 'ejs');

//Use layouts
app.use(expressLayouts);

//ALLOW CROSS-DOMAIN REQUESTS
var allowCrossDomain = function(req, res, next) {
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
	res.header('Access-Control-Allow-Headers', 'Content-Type');
	next();
}

//app.use(allowCrossDomain);

//LANGUAGE SETTINGS
i18n.configure({
	cookie: config.get("localeCookieName"),
	directory: __dirname + '/locales',
	defaultLocale:"en",
	register:global,
	updateFiles: false
});


var moment=require("moment");


//GLOBAL EJS HELPER
app.use(function (req, res, next) {
	res.locals.__ = res.__ = function() {
		return i18n.__.apply(req, arguments);
	};

	//Add user
	if(req.user){
		if(!res.locals.user)
			res.locals.user=req.user;
		//set locale to selected by user
		i18n.setLocale(req.user.language);
	}else{
		if(!res.locals.user)
			res.locals.user=false;
	}

	res.cookie(config.get("localeCookieName"), i18n.getLocale(), { maxAge: 900000 });

	moment.locale(req.cookies[config.get("localeCookieName")]||i18n.getLocale());
	next();
});

//USE LANG INIT
app.use(i18n.init);


var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));


//Routes
var apiRoutes=require("./routes/api");
var authRoutes=require("./routes/auth")(app);
var pagesRoutes=require("./routes/pages")(app);
var donateRoutes=require("./modules/donate/donate")(app);


app.use('/', authRoutes);
app.use('/', pagesRoutes);

app.use('/api/', apiRoutes);
app.use('/donate/',donateRoutes );

app.use('/', express.static(__dirname + '/public'));





//START HTTP SERVER

var fs = require('fs');





app.listen(config.get("port"),function(){})
//
// //Local https
// if(config.get("dev")==true){
// 	var https = require('https');
// 	//CERTIFICATES
// 	var options = {
// 		key: fs.readFileSync('../certificates/31011560-localhost.key'),
// 		cert: fs.readFileSync('../certificates/31011560-localhost.cert')
// 	};
// 	https.createServer(options, app).listen(config.get('port'));
// }else{
// 	app.listen(config.get("port"),function(){})
// }


require("./modules/cron");

module.exports = function(shouldRunCronNow){
	//CRON
	if(shouldRunCronNow){
		require("./modules/cron")(true);
	}
	return app;
};