var passport=require("passport");
var TwitterStrategy  = require('passport-twitter').Strategy;
var FacebookStrategy  = require('passport-facebook').Strategy;
require("express-session");
// load up the user model
var User       = require('../models/user');

// load the auth variables
var configAuth = require('./auth');



module.exports=function(app){
	app.use(passport.initialize());
	app.use(passport.session()); // persistent login sessions


	// used to serialize the user for the session
	passport.serializeUser(function(user, done) {
		done(null, user.id);
	});

// used to deserialize the user
	passport.deserializeUser(function(id, done) {
		User.findById(id, function(err, user) {
			done(err, user);
		});
	});
// =========================================================================
// TWITTER =================================================================
// =========================================================================
	passport.use(new TwitterStrategy({
				consumerKey     : configAuth.twitterAuth.consumerKey,
				consumerSecret  : configAuth.twitterAuth.consumerSecret,
				callbackURL     : configAuth.twitterAuth.callbackURL,
				includeEmail:true,
				includeEntities:false
			},
			function(token, tokenSecret, profile, done) {
				// make the code asynchronous

				// User.findOne won't fire until we have all our data back from Twitter
				process.nextTick(function() {

					var query={$or:[]}

					//Also try to find by email. If email exists - merge user
					if(profile && profile._json && profile._json.email){
						query.$or.push(	{email:profile._json.email})
					}
					query.$or.push({"twitter.id": profile.id});

					User.findOne(query, function(err, user) {

						// if there is an error, stop everything and return that
						// ie an error connecting to the database
						if (err)
							return done(err);

						// if the user is found then log them in
						if (user) {
							user.lastLogin=new Date();

							//Get fresh avatar and fresh email
							user.twitter.avatar=profile._json.profile_image_url_https;
							user.twitter.email=profile._json.email;
							user.twitter.username    = profile.username;
							user.twitter.displayName = profile.displayName;
							user.twitter.id = profile.id;
							user.email=profile._json.email;

							//Initially there was no such property. So check it, why not, we're already re-checking avatar and email each time
							if(!user.created)
								user.created=new Date();

							user.save();
							return done(null, user); // user found, return that user
						} else {
							// if there is no user, create them
							var newUser                 = new User();

							// set all of the user data that we need
							newUser.twitter.id          = profile.id;
							newUser.twitter.token       = token;
							newUser.twitter.username    = profile.username;
							newUser.twitter.avatar    = profile._json.profile_image_url_https;
							newUser.twitter.displayName = profile.displayName;


							newUser.email=profile._json.email;
							newUser.created=new Date();

							// save our user into the database
							newUser.save(function(err) {
								if (err)
									throw err;
								return done(null, newUser);
							});
						}
					});
				});
			}));


	passport.use(new FacebookStrategy({
				clientID: configAuth.facebookAuth.clientID,
				clientSecret: configAuth.facebookAuth.clientSecret,
				callbackURL: configAuth.facebookAuth.callbackURL,
				profileFields: ['id', 'email', 'gender', 'link', 'locale', 'name','photos',"displayName"]
			},
			function(accessToken, refreshToken, profile, done) {
				process.nextTick(function() {

					var query={$or:[]}

					if(profile && profile._json && profile._json.email){
						query.$or.push(	{email:profile._json.email})
					}
					query.$or.push({"facebook.id": profile.id});

					User.findOne(query, function(err, user) {

						// if there is an error, stop everything and return that
						// ie an error connecting to the database
						if (err)
							return done(err);

						// if the user is found then log them in
						if (user) {
							user.lastLogin=new Date();

							user.authProvider="facebook";
							//Get fresh facebook avatar
							if(profile.photos && profile.photos.length)
								user.facebook.avatar=profile.photos[0].value;

							//refresh facebook profile (if we found user by email
							user.facebook.id          = profile.id;
							user.facebook.email=profile._json.email;
							user.facebook.displayName = profile.displayName;
							//If user has email already - save extra email only
							user.email=profile._json.email;


							//Initially there was no such property. So check it, why not, we're already re-checking avatar and email each time
							if(!user.created)
								user.created=new Date();

							user.save();
							return done(null, user); // user found, return that user
						} else {
							// if there is no user, create them
							var newUser                 = new User();

							// set all of the user data that we need
							newUser.facebook.id          = profile.id;

							if(profile.photos && profile.photos.length)
								newUser.facebook.avatar=profile.photos[0].value;

							newUser.facebook.displayName = profile.displayName;
							newUser.facebook.email=profile._json.email;
							newUser.email=profile._json.email;
							newUser.created=new Date();

							// save our user into the database
							newUser.save(function(err) {
								if (err)
									throw err;
								return done(null, newUser);
							});
						}
					});
				});



			}
	));
}

