var gulp = require("gulp");
var gutil = require("gulp-util");
var webpack = require("webpack");
var git = require('gulp-git');
var mjml = require('gulp-mjml');
var rename=require("gulp-rename");
var replace=require("gulp-replace");
var webpackConfig = require("./web/webpack.config.js");
var webpackConfigProd = require("./web/webpack.config.prod.js");
var inlineCss = require('gulp-inline-css');
// The development server (the recommended option for development)
gulp.task("default", ["webpack:build"]);


gulp.task("webpack:build", function(callback) {
	// modify some webpack config options
	var myConfig = Object.create(webpackConfigProd);
	// run webpack
	webpack(myConfig, function(err, stats) {
		if(err) throw new gutil.PluginError("webpack:build", err);
		gutil.log("[webpack:build]", stats.toString({
			colors: true
		}));
		callback();
	});
});


gulp.task("dev", function(callback) {
	// modify some webpack config options
	var myConfig = Object.create(webpackConfig);
	// run webpack
	webpack(myConfig, function(err, stats) {
		if(err) throw new gutil.PluginError("webpack:build", err);
		gutil.log("[webpack:build]", stats.toString({
			colors: true
		}));
		callback();
	});
});

gulp.task('makemail', function() {
	return gulp.src('./modules/mailer/mail-template-original.ejs')
			.pipe(inlineCss({
				applyStyleTags: true,
				applyLinkTags: true,
				removeStyleTags: false,
				removeLinkTags: false
			}))
			.pipe(rename("mail-template.ejs"))

			//REPLACE ALL SPECIAL THINGS TO MAKE IT EJS-ed
			.pipe(replace(/__FIRSTLINE__/,"<%= __('mailNamePre') %> <%= user.twitter.displayName %>"))
			.pipe(replace(/__DONATELINK__/,"<%= donateLink %>"))
			.pipe(replace(/__PROFILELINK__/,"<%= host + '/profile' %>"))

			.pipe(replace(/__MAINCOLOR__/g,"#168ec0"))
			.pipe(replace(/__GRAYCOLOR__/g,"#ececec"))
			.pipe(replace(/__MAINCOLORHOVER__/g,"#16bbf3"))
			.pipe(replace(/__WORDCOUNT__/,"<%= count %>"))

			.pipe(replace(/__DEBT__/,"<%= debt %>"))
			.pipe(replace(/__WORDREPEATERSTART__/,"<% for(var i = 0; i < matchings.length; i++) { %>"))
			.pipe(replace(/__WORDREPEATEREND__/,"<% } %>"))
			.pipe(replace(/__WORDREPEATERWORD__/,"<%= matchings[i].word %>"))

			.pipe(replace(/__WORDREPEATERETYMOLOGY__/,"<%= __('from') %> <%= matchings[i].etymology %>"))

			.pipe(replace(/__LOGO__/,"<%= logo %>"))
			.pipe(replace(/__LOGOUNICEF__/,"<%= logoUnicef %>"))
			.pipe(replace(/__LOGOSMILE__/,"<%= logoSmile %>"))
			.pipe(replace(/__LOGOHEALWORLD__/,"<%= logoHeal %>"))
			.pipe(replace(/__HOST__/,"<%= host %>"))


			//TRANSLATED PARTS
			.pipe(replace(/__INTRO__/,"<%= __('mailMessage') %>"))
			.pipe(replace(/__PAYWITH__/,"<%= __('payWith') %>"))
			.pipe(replace(/__PAYPALLOGO__/,"<img src='<%= paypalLogoUrl %>'>"))
			.pipe(replace(/__PAYOFF__/,"<%= __('payoff') %>"))
			.pipe(replace(/__TOTALWORDSTEXT__/,"<%= __('totalWords') %>"))
			.pipe(replace(/__DEBTTEXT__/,"<%= __('currentDebt') %>"))
			.pipe(replace(/__GREEK__/,"<%= __('greek') %>"))
			.pipe(replace(/__WORDS__/,"<%= __('words') %>"))
			.pipe(replace(/__SEEMORE__/,"<%= __('seeMore') %>"))

			.pipe(rename("mail-template.ejs"))
			.pipe(gulp.dest('views/'));
});


gulp.task('update', function(){
	git.pull('origin', 'master', {args: '--rebase'}, function (err) {
		if (err) throw err;
		gulp.start("default");
	});
});

gulp.task('mjml', function () {
	gulp.src('./modules/mailer/mail-template.mjml')
			.pipe(mjml())
			.pipe(rename("mail-template.ejs"))
			.pipe(replace(/__USERNAME__/,"<%= user.twitter.displayName %>"))
			.pipe(replace(/__DONATELINK__/,"<%= donateLink %>"))
			.pipe(replace(/__MAINCOLOR__/g,"#168ec0"))
			.pipe(replace(/__GRAYCOLOR__/g,"#ececec"))
			.pipe(replace(/__WORDCOUNT__/,"<%= count %>"))

			.pipe(replace(/__SEEMORE__/,"<%= __('seeMore') %>"))
			.pipe(replace(/__DEBT__/,"<%= debt %>"))
			.pipe(replace(/__WORDREPEATERSTART__/,"<% for(var i = 0; i < matchings.length; i++) { %>"))
			.pipe(replace(/__WORDREPEATEREND__/,"<% } %>"))
			.pipe(replace(/__WORDREPEATERWORD__/,"<%= matchings[i].word %>"))

			.pipe(replace(/__WORDREPEATERETYMOLOGY__/,"<%= matchings[i].etymology %>"))
			.pipe(replace(/__WORDREPEATERMEANING__/,"<%= matchings[i].meaning %>"))

			.pipe(replace(/__LOGOURL__/,"<%= logo %>"))
			.pipe(replace(/__LOGOUNICEFURL__/,"<%= logoUnicef %>"))
			.pipe(replace(/__HOST__/,"<%= host %>"))

			//FONT AND IT'S SIZES
			.pipe(replace(/__FONTFAMILY__/g,"Roboto"))
			.pipe(replace(/__FONTSMALL__/g,"12px"))
			.pipe(replace(/__FONTNORMAL__/g,"18px"))
			.pipe(replace(/__FONTBIGGER__/g,"20px"))
			.pipe(replace(/__FONTHUGE__/g,"50px"))

			//TRANSLATED PARTS
			.pipe(replace(/__NAMEPRE_/,"<%= __('mailNamePre') %>"))
			.pipe(replace(/__INTRO__/,"<%= __('mailMessage') %>"))
			.pipe(replace(/__FROM__/,"<%= __('from') %>"))
			.pipe(replace(/__PAYWITH__/,"<%= __('payWith') %> <img src='<%= paypalLogoUrl %>'>"))
			.pipe(replace(/__PAYPALLOGOURL__/,"<%= paypalLogoUrl %>"))
			.pipe(replace(/__PAYOFF__/,"<%= __('payoff') %>"))
			.pipe(replace(/__TOTALWORDSTEXT__/,"<%= __('totalWords') %>"))
			.pipe(replace(/__DEBTTEXT__/,"<%= __('currentDebt') %>"))
			.pipe(replace(/__GREEK__/,"<%= __('greek') %>"))
			.pipe(replace(/__WORDS__/,"<%= __('words') %>"))


			.pipe(gulp.dest('./views/'))
});


