// app/models/user.js
// load the things we need
var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');

var moment=require("moment");
var config      = require('../config/config');

var tooOldDate="1980-01-01";

// define the schema for our user model
var userSchema = mongoose.Schema({

	local            : {
		email        : String,
		password     : String
	},
	twitter          : {
		id           : String,
		token        : String,
		displayName  : String,
		username     : String,
		email        : String,
		avatar:String
	},
	facebook:{
		id:String,
		displayName  : String,
		email:String,
		avatar:String
	},
	email:String,
	"created":{type:Date},
	"language":{type:String,default:"en"},
	"continent_code":String,
	lastDonation:{type:Date,default:new Date(tooOldDate)},
	lastNotification:{type:Date,default:new Date(tooOldDate)},
	lastLogin:{type:Date}
});

// methods ======================
// generating a hash
userSchema.methods.generateHash = function(password) {
	return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
userSchema.methods.validPassword = function(password) {
	return bcrypt.compareSync(password, this.local.password);
};


userSchema.statics.findScheduledUsers=function(callback){
	//Find all below
	var findBelow=moment().subtract(config.get("cron").tweetScanFrequency,"days").endOf("day");
	return this.find({$or:[
		{"lastNotification":{"$lte":new Date(findBelow.toISOString())}},
		{"lastNotification":{"$exists":false}}
	]

	},callback);
}

// create the model for users and expose it to our app
module.exports = mongoose.model('User', userSchema);