
var mongoose = require('mongoose');
var _= require("lodash");
// define the schema for our user model
var WordUsageSchema = mongoose.Schema({
	word:{type:String,index: { unique: true }},
	total:{type:Number,default:0},
	etymology:{type:String},
	meaning:{type:String},
	continents:{
		na:{type:Number,default:0},
		sa:{type:Number,default:0},
		eu:{type:Number,default:0},
		af:{type:Number,default:0},
		oc:{type:Number,default:0},
		as:{type:Number,default:0}
	}
});





// create the model for users and expose it to our app
module.exports = mongoose.model('WordUsage', WordUsageSchema);