
var mongoose = require('mongoose');
var _= require("lodash");
// define the schema for our user model
var GreekRootsSchema = mongoose.Schema({
	roots:{type:[String]},
	meaning:String,
	etymology:String,
	matching:{type:[String]},
	ru:{
		roots:{type:[String]},
		meaning:String,
		etymology:String,
		matching:{type:[String]}
	},
	fr:{
		roots:{type:[String]},
		meaning:String,
		etymology:String,
		matching:{type:[String]}
	},
	es:{
		roots:{type:[String]},
		meaning:String,
		etymology:String,
		matching:{type:[String]}
	},
	de:{
		roots:{type:[String]},
		meaning:String,
		etymology:String,
		matching:{type:[String]}
	}
});

var rootLocalesList=["ru","fr","es","de"];


GreekRootsSchema.statics.findMatching=function(text,cb){

	var result={
		total:0,
		matchings:{}
	}

	if(!text && cb)
		return cb(false,result);

	words=text.split(" ").map(function(word){
		word=word.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '');
		//word=encodeURI(word);
		return word.length && word;
	});


	var countMap={};
	//Take counters map for all words (find duplicates here)
	words.forEach(function(word){
		if(!countMap[word])
			countMap[word]=1;
		else
			countMap[word]++;
	});

	var commonIn={$in:words};
	var commonCriteria={matching:commonIn};

	var searchQuery={
		$or:[
			commonCriteria
		]
	}

	rootLocalesList.forEach(function(lang){
		var newOr={};
		newOr[lang+".matching"]=commonIn;
		searchQuery.$or.push(newOr);
	});

	var extractMatches=function(matches,root){
		matches.forEach(function(m){
			result.matchings[m]=root.toObject();

			result.matchings[m]["word"]=m;
			result.matchings[m]["count"]=countMap[m];

			result.total+=countMap[m]||0;
		})
	}

	return this.find(searchQuery,function (err, roots) {
		if (!err) {
			roots.forEach(function(root){

				var match=_.intersection(words,root.matching);

				//Extract matches from main language
				extractMatches(match,root);

				//Extract matches from other languages
				rootLocalesList.forEach(function (l) {
					if(root[l] && root[l].matching && root[l].matching.length){
						extractMatches(_.intersection(words,root[l].matching),root[l]);
					}
				})

			})
			return cb && cb(false,result);

		} else {
			return cb && cb(err);
		}
	});
}



// create the model for users and expose it to our app
module.exports = mongoose.model('GreekRoots', GreekRootsSchema);