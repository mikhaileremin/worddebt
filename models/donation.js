var mongoose = require('mongoose');
Schema        =       mongoose.Schema;

// define the schema for our user model
var donationSchema = mongoose.Schema({
	user:{ type: Schema.Types.ObjectId, ref: 'User' },
	transaction:Object,
	invoice:{type:String,unique:true},
	date:{type:Date,default:new Date()},
});

// create the model for users and expose it to our app
module.exports = mongoose.model('Donation', donationSchema);